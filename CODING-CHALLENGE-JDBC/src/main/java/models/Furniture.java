package models;

public class Furniture {
	private int furnitureid;
	private int homeid;
	private String furniturename;
	private int furnituresize;
	
	public Furniture(int furnitureid, int homeid, String furniturename, int furnituresize) {
		this.furnitureid = furnitureid;
		this.homeid = homeid;
		this.furniturename = furniturename;
		this.furnituresize = furnituresize;
	}
	
	public int getHomeId() {
		return homeid;
	}
	
	public int getFurnitureId() {
		return furnitureid;
	}
	
	public String getFurnitureName() {
		return furniturename;
	}
	
	public int getFurnitureSize() {
		return furnituresize;
	}
}
