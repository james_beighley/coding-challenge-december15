package models;

public class Home {
	private int homeid;
	private String homename;
	private int homesize;
	private int usedsize;
	
	public Home(int homeid, String homename, int homesize, int usedsize) {
		this.usedsize = usedsize;
		this.homeid = homeid;
		this.homename = homename;
		this.homesize = homesize;
	}
	
	public int getHomeId() {
		return homeid;
	}
	
	public int getHomeSize() {
		return homesize;
	}
	
	public String getHomeName() {
		return homename;
	}
	
	public int getUsedSize() {
		return usedsize;
	}
	
	public void setUsedSize(int newsize) {
		usedsize = newsize;
	}
}
