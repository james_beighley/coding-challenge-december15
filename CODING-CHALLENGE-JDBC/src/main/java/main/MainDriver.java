package main;

import dao.HomeDaoImpl;

import java.util.ArrayList;
import java.util.Scanner;
import models.Furniture;
import models.Home;
import service.HomeServiceImpl;

public class MainDriver {
	static HomeServiceImpl service = new HomeServiceImpl();
	static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		boolean stay = true;
		while(stay) {
			ArrayList<Home> homes = service.getAllHomes();
			for(int i = 0; i< homes.size(); i++) {
				System.out.println("id: " + homes.get(i).getHomeId() + " name: " + homes.get(i).getHomeName() +" total space: " + homes.get(i).getHomeSize()+"sqft" + " available space: " + homes.get(i).getUsedSize()+"sqft");
			}
			System.out.println("Type '1' to create a home, '2' edit current homes furniture, '3' to delete a home, anything else to quit");
			String input = scan.nextLine();
			
			//case where we add a new home
			
			if(input.equals("1")) {
				newHome();
			}
			
			//case where we edit contents of a house
			
			else if(input.equals("2")){
				boolean stay2 = true;
				System.out.println("Type the home id to look at its furniture");
				int thisHome = scan.nextInt();
				scan.nextLine();
				while(stay2) {
					ArrayList<Furniture> myfurniture = service.getFurniturebyId(thisHome);
					for(int i = 0; i< myfurniture.size(); i++) {
						System.out.println("id: " + myfurniture.get(i).getFurnitureId() + " name: " + myfurniture.get(i).getFurnitureName() + " size: " + myfurniture.get(i).getFurnitureSize() +"sqft");
					}
					System.out.println("Type '1' to add furniture, '2' to delete, anything else to go back");
					String myAnswer = scan.nextLine();
					
					//case where we delete furniture
					if(myAnswer.equals("2")) {
						System.out.println("Type the id of the furniture to delete");
						int toDelete = scan.nextInt();
						scan.nextLine();
						service.deleteFurniture(toDelete);
					}
					
					//case where we add furniture
					else if(myAnswer.equals("1")) {
						newFurniture(thisHome);
					}
					
					//case where we go back
					
					else {
						stay2 = false;
					}
				}

			}
			
			//case where we delete a home
			
			else if(input.equals("3")){
				System.out.println("Select a home by id to delete");
				int hometodelete = scan.nextInt();
				scan.nextLine();
				service.deleteHome(hometodelete);
			}
			else {
				stay = false;
			}
		}
		
	}
	
	/**
	 * main method invoked to add a new home to the database
	 */
	
	public static void newHome() {
		System.out.println("Enter your home name");
		String home_name = scan.nextLine();
		System.out.println("Enter your home size");
		int home_size = scan.nextInt();
		scan.nextLine();
		service.createHome(home_name, home_size);
	}
	
	/**
	 * newFurniture takes input from the user and uses it store new
	 * furniture in the furniture table
	 * @param homeid
	 */
	
	public static void newFurniture(int homeid) {
		System.out.println("Enter your furniture name");
		String furniture_name = scan.nextLine();
		System.out.println("Enter your furniture size");
		int furniture_size = scan.nextInt();
		service.addFurniture(homeid, furniture_name, furniture_size);
	}

}
