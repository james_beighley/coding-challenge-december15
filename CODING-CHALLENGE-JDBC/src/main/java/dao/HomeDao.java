package dao;

import models.Home;
import models.Furniture;
import java.util.ArrayList;

public interface HomeDao {
	public void createHome(String home_name, int home_size);
	public void insertFurniture(int homeid, String furniturename, int furnituresize);
	public void deleteFurniture(int furnitureid);
	public ArrayList<Home> getAllHomes();
	public Home getHomeById(int id);
	public ArrayList<Furniture> getFurnitureById(int id);
	public void updateHome(int homeid, String homename, int homesize, int usedsize);
	public void DeleteHomeById(int id);
}
