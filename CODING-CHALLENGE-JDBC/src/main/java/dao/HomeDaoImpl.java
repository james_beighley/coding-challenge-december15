package dao;

import java.util.ArrayList;
import models.Furniture;
import models.Home;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class HomeDaoImpl implements HomeDao {
	
	public static String url = "";
	public static String username= "";
	public static String password="";

	/**
	 * create home makes a new home with a name and a given size
	 * @param home_name
	 * @param home_size
	 */
	@Override
	public void createHome(String home_name, int home_size) {
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			

			
			String sql = "INSERT INTO Home (home_id, home_name, home_size, used_size) VALUES(DEFAULT, ?, ?, ?)";
			

			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, home_name); 
			ps.setInt(2, home_size);
			ps.setInt(3, home_size/2);
			
			ps.executeUpdate(); 
		
		}catch(SQLException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * create home makes a new home with a name and a given size
	 * @param homeid
	 * @param furniturename
	 * @param furnituresize
	 */

	@Override
	public void insertFurniture(int homeid, String furniturename, int furnituresize) {
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			

			
			String sql = "INSERT INTO Furniture(furniture_id, home_id, furniture_name, furniture_size) VALUES(DEFAULT, ?, ?, ?)";
			

			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, homeid); 
			ps.setString(2, furniturename);
			ps.setInt(3, furnituresize);
			ps.executeUpdate(); 
		
		}catch(SQLException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * delete furniture from the furniture table with furnitureid
	 * @param furnitureid
	 */

	@Override
	public void deleteFurniture(int furnitureid) {
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "DELETE FROM Furniture WHERE furniture_id = " + "'" +furnitureid+ "'";
			
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);

			
		
		}catch(SQLException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * get every home from the current home table
	 */

	@Override
	public ArrayList<Home> getAllHomes() {
		ArrayList<Home> homes = new ArrayList<>();
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "SELECT * FROM Home";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery(); //<--query not update
			
			while(rs.next()) {
				homes.add(new Home(rs.getInt(1), rs.getString(2), rs.getInt(3),rs.getInt(4)));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return homes;
	}
	
	/**
	 * get single home by the home id from the current home table
	 * @param id
	 */

	@Override
	public Home getHomeById(int id) {
		Home toreturn = null;
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "SELECT * FROM Home WHERE home_id =" + "'" +id+ "'";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery(); //<--query not update
			
			while(rs.next()) {
				toreturn = new Home(rs.getInt(1), rs.getString(2), rs.getInt(3),rs.getInt(4));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
	
		return toreturn;
	}
	
	/**
	 * get every home from the current home table
	 * @param id
	 */

	@Override
	public ArrayList<Furniture> getFurnitureById(int id) {
		ArrayList<Furniture> allfurniture = new ArrayList<>();
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "SELECT * FROM Furniture WHERE home_id =" + "'" +id+ "'";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery(); //<--query not update
			
			while(rs.next()) {
				allfurniture.add(new Furniture(rs.getInt(1), rs.getInt(2), rs.getString(3),rs.getInt(4)));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return allfurniture;
	}
	
	/**
	 * use this to update the used space of the home after deleting inserting furniture
	 * @param homeid
	 * @param homename
	 * @param homesize
	 * @param usedsize
	 */

	@Override
	public void updateHome(int homeid, String homename, int homesize, int usedsize) {
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "UPDATE home SET used_size=" + "'" +usedsize  + "'" + " WHERE home_id = "+ "'" +homeid +"'";
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);

			
		
		}catch(SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void DeleteHomeById(int id) {
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "DELETE FROM home WHERE home_id = " + "'" +id+ "'";
			
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);

			
		
		}catch(SQLException e) {
			e.printStackTrace();
		}

	}

}
