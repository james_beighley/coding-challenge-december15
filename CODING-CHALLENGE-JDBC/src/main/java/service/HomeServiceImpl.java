package service;

import dao.HomeDaoImpl;
import java.util.ArrayList;
import models.Furniture;
import models.Home;

public class HomeServiceImpl {
	
	HomeDaoImpl dao = new HomeDaoImpl();
	
	/**
	 * Use this to create a new home
	 * @param home_name
	 * @param home_size
	 */
	
	public void createHome(String home_name, int home_size) {
		dao.createHome(home_name, home_size);
	}
	
	/**Use this to get a list of all homes
	 * 
	 * @return
	 */
	
	public ArrayList<Home> getAllHomes() {
		return dao.getAllHomes();
	}
	
	/**use this method to delete your home, deletes all furniture in home
	 * 
	 * @param id
	 */
	
	public void deleteHome(int id) {
		ArrayList<Furniture> myfurniture = dao.getFurnitureById(id);
		for(int i = 0; i< myfurniture.size(); i++) {
			dao.deleteFurniture(myfurniture.get(i).getFurnitureId());
		}
		dao.DeleteHomeById(id);
	}
	
	/**use this method to add furniture specifying name and size
	 * to a home with a given id
	 * @param homeid
	 * @param furniturename
	 * @param furnituresize
	 * @return
	 */
	
	
	public boolean addFurniture(int homeid, String furniturename, int furnituresize) {
		Home thisHome = dao.getHomeById(homeid);
		if(thisHome.getUsedSize()-furnituresize<0) {
			System.out.println("Not enough space to add furniture");
			return false;
		}
		else {
			dao.insertFurniture(homeid, furniturename, furnituresize);
			int updatedusedspace = thisHome.getUsedSize() - furnituresize;
			dao.updateHome(homeid, thisHome.getHomeName(), thisHome.getHomeSize(),updatedusedspace);
			return true;
		}
			
	}
	
	/**
	 * Get all furniture associated with a certain home's id
	 * @param homeid
	 * @return
	 */
	
	public ArrayList<Furniture> getFurniturebyId(int homeid) {
		return dao.getFurnitureById(homeid);
	}
	
	/**
	 * delete furniture using that furniture's id
	 * @param id
	 */
	
	public void deleteFurniture(int id) {
		dao.deleteFurniture(id);
	}
	
	
}
